﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinAndMaxFunction_GeneticAlgorithm
{
    class ParameterFunction
    {
        //x i y koordinate
        private double x, y;
        //x i y koordinate po cijelom broju u intervalu
        private int xInt, yInt;

        //getteri i setteri
        public double X { get => x; set => x = value; }
        public double Y { get => y; set => y = value; }
        public int XInt { get => xInt; set => xInt = value; }
        public int YInt { get => yInt; set => yInt = value; }

        //racunanje x i y u integer
        public void Db()
        {
            xInt = (int)Math.Floor((x - ParameterAlgorithm.GDx) * (Math.Pow(2, ParameterAlgorithm.BitsNumberX) - 1) / (ParameterAlgorithm.GGx - ParameterAlgorithm.GDx));
            yInt = (int)Math.Floor((y - ParameterAlgorithm.GDy) * (Math.Pow(2, ParameterAlgorithm.BitsNumberY) - 1) / (ParameterAlgorithm.GGy - ParameterAlgorithm.GDy));
        }
        public void DXY()
        {
            x = Math.Round(ParameterAlgorithm.GDx + (ParameterAlgorithm.GGx - ParameterAlgorithm.GDx)*(xInt) / (Math.Pow(2, ParameterAlgorithm.BitsNumberX) - 1), ParameterAlgorithm.Precision);
            y = Math.Round(ParameterAlgorithm.GDy + (ParameterAlgorithm.GGy - ParameterAlgorithm.GDy)*(yInt) / (Math.Pow(2, ParameterAlgorithm.BitsNumberY) - 1), ParameterAlgorithm.Precision);
        }
    }
}
