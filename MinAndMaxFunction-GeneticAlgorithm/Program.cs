﻿using System;
using System.Collections.Generic;

namespace MinAndMaxFunction_GeneticAlgorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            ParameterAlgorithm.GDx = -3;
            ParameterAlgorithm.GGx = 3;
            ParameterAlgorithm.GDy = -3;
            ParameterAlgorithm.GGy = 3;
            ParameterAlgorithm.Precision = 2;
            ParameterAlgorithm.ProbabilityCrossover = 0.6;
            ParameterAlgorithm.MaxIteration = 100;
            ParameterAlgorithm.MaxIterationRepeat = 2;
            ParameterAlgorithm.ProbabilityMutation = 0.1;
            ParameterAlgorithm.PopulationNumber = 64;
            ParameterAlgorithm.SetBitsXY();
            DateTime startTime = DateTime.Now;
            ParameterAlgorithm.Population = ParameterAlgorithm.InitializationTest();
            for (int i = 0, j=0; i < ParameterAlgorithm.MaxIteration && j<ParameterAlgorithm.MaxIterationRepeat; i++)
            {
                if (!isEqualsParameter(ParameterAlgorithm.Population))
                {
                    Console.WriteLine("Iteration: " + i);
                    List<double> resultFunction = ParameterAlgorithm.GetResultOfFunction(ParameterAlgorithm.Population);
                    List<double> fittnes = ParameterAlgorithm.FitnessFuntionForMinimum(resultFunction, out double popultationRate);
                    List<double> rulet = ParameterAlgorithm.RouletteCalc(fittnes, popultationRate);
                    List<double> cumultuv = ParameterAlgorithm.CumulativeProbabilityCalc(rulet);
                    List<ParameterFunction> selectionPop = ParameterAlgorithm.GetPopulationWithRoulette(cumultuv, ParameterAlgorithm.Population);
                    List<ParameterFunction> listamix = ParameterAlgorithm.MixChromosome(selectionPop, ParameterAlgorithm.PopulationNumber /2);
                    List<ParameterFunction> crossover = ParameterAlgorithm.CrossoverChromosome(listamix);
                    if (i < ParameterAlgorithm.MaxIteration - 1 && j < ParameterAlgorithm.MaxIterationRepeat - 1)
                    {
                        List<ParameterFunction> mutation = ParameterAlgorithm.MutationChromosomes(crossover);
                        ParameterAlgorithm.Population = mutation;
                    }
                    foreach (var param in ParameterAlgorithm.Population)
                        Console.WriteLine("X= " + param.X + " ; Y= " + param.Y + " ;Func(x,y)= " + ParameterAlgorithm.function(param));

                }
                else
                {
                    j++;
                    Console.WriteLine("ispis j="+j);
                    ParameterAlgorithm.Population=ParameterAlgorithm.MutationChromosomes(ParameterAlgorithm.Population);
                    foreach (var param in ParameterAlgorithm.Population)
                        Console.WriteLine("X= " + param.X + " ; Y= " + param.Y + " ;Func(x,y)= " + ParameterAlgorithm.function(param));
                }
                
            }
            double endTime = DateTime.Now.Subtract(startTime).TotalMilliseconds;
            Console.WriteLine("Vrijeme izvršavanja je: "+endTime);
            
        }
        private static bool isEqualsParameter(List<ParameterFunction> parameters)
        {
            ParameterFunction param = parameters[0];
            List<ParameterFunction> list= parameters.FindAll((a)=> (a.X.Equals(param.X) && a.Y.Equals(param.Y)) || (a.Y.Equals(param.X) && a.X.Equals(param.Y)));
            if (parameters.Count.Equals(list.Count)) return true;
            return false;
        }
    }
}
