﻿using System;
using System.Collections.Generic;

namespace MinAndMaxFunction_GeneticAlgorithm
{
    class ParameterAlgorithm
    {
        //preciznost 10^(-precision)
        private static int precision;
        //maksimalan broj iteracija
        private static int maxIteration;
        //maksimalan broj iteracija kad se ponavljaju
        private static int maxIterationRepeat;
        //vjerovatnoca mutacije
        private static double probabilityMutation;
        //vjerovatnoca ukrstanja
        private static double probabilityCrossover;
        //populacija
        private static List<ParameterFunction> population;
        //broj bita za x osu
        private static int bitsNumberX;
        //broj bita za y osu
        private static int bitsNumberY;
        //broj pocetne populacije
        private static int populationNumber;
        //gornje i donje granice intervala po x i po y
        private static int gDx, gGx, gDy, gGy;
        //radnom generator
        private static Random rand = new Random();
        public static int Precision { get => precision; set => precision = value; }
        public static int MaxIteration { get => maxIteration; set => maxIteration = value; }
        public static double ProbabilityMutation { get => probabilityMutation; set => probabilityMutation = value; }
        public static double ProbabilityCrossover { get => probabilityCrossover; set => probabilityCrossover = value; }
        public static int BitsNumberX { get => bitsNumberX; set => bitsNumberX = value; }
        public static int PopulationNumber { get => populationNumber; set => populationNumber = value; }
        public static int BitsNumberY { get => bitsNumberY; set => bitsNumberY = value; }
        public static int GDx { get => gDx; set => gDx = value; }
        public static int GGx { get => gGx; set => gGx = value; }
        public static int GDy { get => gDy; set => gDy = value; }
        public static int GGy { get => gGy; set => gGy = value; }
        internal static List<ParameterFunction> Population { get => population; set => population = value; }
        public static int MaxIterationRepeat { get => maxIterationRepeat; set => maxIterationRepeat = value; }

        public static void SetBitsXY()
        {
            //racunanje koliko je potrebno bitova za prestavljanje decimalnih vrijendosti po x i y kooridanti
            BitsNumberX = (int)Math.Ceiling(Math.Log(((gGx - gDx) * Math.Pow(10, precision)) + 1, 2));
            BitsNumberY = (int)Math.Ceiling(Math.Log(((gGy - gDy) * Math.Pow(10, precision)) + 1, 2));
        }
        public static double GetRandomDouble(int gD, int gG)
        {
            //nextDouble vraca vrijednost vecu ili jednaku 0 a manju od nula pa je prvi interval [0,1]
            //tako da gg-gd kod tog intervala je 1 pa za k dobijemo da je samo g'g - g'd
            //a za n g'd*gg=g'd jer je gg=1 a g'g*gd=0 jer je gd=0 pa je k=g'd pa i onda dobijemo y=(g'g-g'd)+g'd
            return rand.NextDouble() * (gG - gD) + gD;
        }
        public static List<double> FitnessFuntionForMaximum(List<double> listFunctionResults, out double populatioRate)
        {
            //nadjemo minimum liste pozivom funcije findMinimum
            double minimum = FindMinimum(listFunctionResults);
            populatioRate = 0;
            List<double> result = new List<double>();
            //svaku vrijednost iz liste radimo sljedece
            foreach (var item in listFunctionResults)
            {
                //izracunamo vrijednost broja iz liste i oduzmemo vrijednost minimuma
                double resultIteration = item - minimum;
                //dodamo rezultat fitnes funckije za datu vrijednost u listu
                result.Add(resultIteration);
                //saberemo vrijednost u totalnu ocijenu populacije
                populatioRate += resultIteration;
            }
            return result;
        }
        public static List<Double> FitnessFuntionForMinimum(List<double> listFunctionResults, out double populatioRate)
        {
            //nadjemo minimum liste pozivom funcije findMinimum
            double maximum = FindMiximum(listFunctionResults);
            Console.WriteLine($"maximum je {maximum}");
            populatioRate = 0;
            List<double> result = new List<double>();
            //svaku vrijednost iz liste radimo sljedece
            foreach (var item in listFunctionResults)
            {
                //izracunamo vrijednost broja iz liste i oduzmemo vrijednost od maksimuma
                double resultIteration = maximum - item;
                //dodamo rezultat fitnes funckije za datu vrijednost u listu
                result.Add(resultIteration);
                //saberemo vrijednost u totalnu ocijenu populacije
                populatioRate += resultIteration;
            }

            return result;
        }
        public static List<Double> RouletteCalc(List<double> fitnessResult,double populatioRate)
        {
            //racunamo velicinu sirine odjeljka zbog ruletske selekcije
            List<double> probability = new List<double>();
            //za svaku izarucantu vrijednost fittnes funkcije racunamo
            foreach(var item in fitnessResult)
            {
                //pi=ff(xi)/F
                probability.Add(item/populatioRate);
            }
            return probability;
        }
        public static List<Double> CumulativeProbabilityCalc(List<double> rouletteProbability)
        {
            //racunamo kumulativnu vjerovatnocu za svaku vrijednost pi=ff(xi)/F
            List<double> CumulativeProbability = new List<double>();
            for (int i = 0; i < rouletteProbability.Count; i++)
            {
                //uzimamo svaku sljedecu vrijednost kolone i sabiramo zbirom vrijednosti do te koline i stavljamo u listu
                double resultInter = 0;
                for (int j = 0; j <= i; j++)
                {
                    resultInter += rouletteProbability[j];
                }
                CumulativeProbability.Add(resultInter);
            }
            return CumulativeProbability;
        }
        private static double FindMinimum(List<double> items)
        {
            //alociramo i kopiramo sadrzaj iz liste da ne bi remetili redoslijed 
            double[] list=new double[items.Count];
            items.CopyTo(list);
            List<double> copyList = new List<double>(list);
            //sortiramo u rastucem redoslijedu
            copyList.Sort();
            //vratimo prvi u listu jer je najmanji broj
            return copyList[0];
        }
        private static double FindMiximum(List<double> items)
        {
            //alociramo i kopiramo sadrzaj iz liste da ne bi remetili redoslijed 
            double[] list = new double[items.Count];
            items.CopyTo(list);
            List<double> copyList = new List<double>(list);
            //sortiramo u rastucem redoslijedu
            copyList.Sort();
            //vratimo zadnji u listu jer je najveci broj
            return copyList[copyList.Count-1];
        }
        public static List<ParameterFunction> GetPopulationWithRoulette(List<double> CumulativeProbability,List<ParameterFunction> population)
        {
            //simlacija okretanja ruleta
            List<ParameterFunction> selectNextPopulation = new List<ParameterFunction>();
            //vrtimo rulet koliko puta je vrijednost populationNumber
            for (int i = 0; i < populationNumber; i++)
            {
                //biramo broj iz [0,1]
                double probabilityIter = rand.NextDouble();
                for (int j = 0; j < CumulativeProbability.Count; j++)
                {
                    //ako je izabrani slucajan broj manji od kumulativne vjerovatnoca za j clan iz liste 
                    //tj. ako je izabrani slucajan broj u intervalu kojem pripada j-ti hromozom
                    if (probabilityIter < CumulativeProbability[j])
                    {
                        //dodajemo u medjuselekciju
                        selectNextPopulation.Add(population[j]);
                        break;
                    }
                }
            }
            return selectNextPopulation;
        }
        public static List<ParameterFunction> MixChromosome(List<ParameterFunction> chromosomes, int mixNumber)
        {
            Console.WriteLine("count: " + chromosomes.Count);
            for (int i = 0; i < mixNumber; i++)
            {
                //uzimanje random pozicije za zamjenu
                int position1 = rand.Next(chromosomes.Count), position2 = rand.Next(chromosomes.Count);
                //uzimanje iz liste hromozome koji se mjenjaju
                ParameterFunction chromesome1 = chromosomes[position1], chromesome2 = chromosomes[position2];
                //zamjena mjesta
                chromosomes.RemoveAt(position1);
                chromosomes.Insert(position1, chromesome2);
                chromosomes.RemoveAt(position2);
                chromosomes.Insert(position2, chromesome1);
            }
            List<ParameterFunction> newList = new List<ParameterFunction>();
            foreach (var item in chromosomes) newList.Add(new ParameterFunction { X = item.X, Y = item.Y, XInt = item.XInt, YInt = item.YInt });
            return newList;

        }
        public static List<ParameterFunction> CrossoverChromosome(List<ParameterFunction> chromosomes)
        {
            for(int i = 0; i < chromosomes.Count ; i+=2)
            {
                double probabilityCrossoverIter = rand.NextDouble();
                if (probabilityCrossoverIter < ProbabilityCrossover)
                {
                    ParameterFunction param1 = chromosomes[i];
                    ParameterFunction param2 = chromosomes[i + 1];
                    ChangeBitsCrossover(ref param1, chromosome2: ref param2 );
                }
            }
            List<ParameterFunction> newList = new List<ParameterFunction>();
            foreach(var item in chromosomes) newList.Add(new ParameterFunction { X = item.X, Y = item.Y, XInt = item.XInt, YInt = item.YInt });
            return newList;
        }

        public static void ChangeBitsCrossover(ref ParameterFunction chromosome1, ref ParameterFunction chromosome2)
        {
            //biramo slucajnu tacku ukrstanja
            int dotCrossover = rand.Next((bitsNumberX <= BitsNumberY ? bitsNumberX : bitsNumberY) - 1), oneBits = 1;
            //racunamo cijelobrojnu vrijednost x i y koordinate
            chromosome1.Db();
            chromosome2.Db();
            //setuje onebits na vrijednost 1111.. u zavosnosti od broj u kojem se ukrstanu
            //npr ako je u tacki 5 dobijamo za onebits vrijednost 111111 koja ce posluziti da dobijanje nizih bita za ukrstanje
            for(int i=0;i<dotCrossover;i++)
            {
                oneBits <<= 1;
                oneBits |= 1;
            }

            //dobijamo od oba hromozoma nize bite od x i y 
            int firstChromosomeBitsX1 = chromosome1.XInt & oneBits, firstChromosomeBitsY1= chromosome1.YInt & oneBits;
            int firstChromosomeBitsX2 = chromosome2.XInt & oneBits, firstChromosomeBitsY2= chromosome2.YInt & oneBits;
            //onebits setujemo tako da vrijednost bude sve jedinice
            oneBits = -1;
            //pomjeramo u lijevo za vrijednost tacke ukrstanja npr za tacku ukrstanja 5 dobijamo ..11 1100 0000
            oneBits <<= dotCrossover+1;

            //setujemo nize bite  x i y prvog hromozoma na 0
            chromosome1.XInt &= oneBits;
            chromosome1.YInt &= oneBits;
            //setujemo nize bite x i y prvog na nize bite drugog hromozoma
            chromosome1.XInt |= firstChromosomeBitsX2;
            chromosome1.YInt |= firstChromosomeBitsY2;

            //setujemo nize bite  x i y drugog hromozoma na 0
            chromosome2.XInt &= oneBits;
            chromosome2.YInt &= oneBits;
            //setujemo nize bite x i y drugog na nize bite prvog hromozoma
            chromosome2.XInt |= firstChromosomeBitsX1;
            chromosome2.YInt |= firstChromosomeBitsY1;

            //postavljamo double vrijednost x i y iz cijelobrojnih vrijednosti
            chromosome1.DXY();
            chromosome2.DXY();
        }
        //funkcija u kojoj se ukrstanje radi u razlicitim tackama za x i y koordinatu
        public static void ChangeBitsCrossoverXYDiff(ref ParameterFunction chromosome1, ref ParameterFunction chromosome2)
        {
            //biramo slucajnu tacku ukrstanja
            int dotCrossoverX = rand.Next(bitsNumberX  - 1), oneBitsX = 1;
            int dotCrossoverY = rand.Next(BitsNumberY - 1), oneBitsY = 1;
            //racunamo cijelobrojnu vrijednost x i y koordinate
            chromosome1.Db();
            chromosome2.Db();
            //setuje onebits na vrijednost 1111.. u zavosnosti od broj u kojem se ukrstanu
            //npr ako je u tacki 5 dobijamo za onebits vrijednost 111111 koja ce posluziti da dobijanje nizih bita za ukrstanje
            for (int i = 0; i < dotCrossoverX; i++)
            {
                oneBitsX <<= 1;
                oneBitsX |= 1;
            }
            for (int i = 0; i < dotCrossoverY; i++)
            {
                oneBitsY <<= 1;
                oneBitsY |= 1;
            }
            //dobijamo od oba hromozoma nize bite od x i y 
            int firstChromosomeBitsX1 = chromosome1.XInt & oneBitsX, firstChromosomeBitsY1 = chromosome1.YInt & oneBitsY;
            int firstChromosomeBitsX2 = chromosome2.XInt & oneBitsX, firstChromosomeBitsY2 = chromosome2.YInt & oneBitsY;
            //onebits setujemo tako da vrijednost bude sve jedinice
            oneBitsX = -1;
            oneBitsY = -1;
            //pomjeramo u lijevo za vrijednost tacke ukrstanja npr za tacku ukrstanja 5 dobijamo ..11 1100 0000
            oneBitsX <<= dotCrossoverX + 1;
            oneBitsY <<= dotCrossoverY + 1;

            //setujemo nize bite  x i y prvog hromozoma na 0
            chromosome1.XInt &= oneBitsX;
            chromosome1.YInt &= oneBitsY;
            //setujemo nize bite x i y prvog na nize bite drugog hromozoma
            chromosome1.XInt |= firstChromosomeBitsX2;
            chromosome1.YInt |= firstChromosomeBitsY2;

            //setujemo nize bite  x i y drugog hromozoma na 0
            chromosome2.XInt &= oneBitsX;
            chromosome2.YInt &= oneBitsY;
            //setujemo nize bite x i y drugog na nize bite prvog hromozoma
            chromosome2.XInt |= firstChromosomeBitsX1;
            chromosome2.YInt |= firstChromosomeBitsY1;

            //postavljamo double vrijednost x i y iz cijelobrojnih vrijednosti
            chromosome1.DXY();
            chromosome2.DXY();
        }
        //funkcija u kojoj x i y mutiraju u istoj tacki
        private static void Mutation(ref ParameterFunction chromosome)
        {
            //imamo bit mutacije slucajno
            int dotMutation = rand.Next(bitsNumberX<=BitsNumberY?bitsNumberX:bitsNumberY),oneBits=1;
            //racunamo cijebrojnu vrijednost x i y
            chromosome.Db();
            //setujemo onebits  na 1 bit koji zelimo da komplemeniramo 
            oneBits <<= dotMutation;
            //komplementiramo ksorovanjem
            chromosome.XInt ^= oneBits;
            chromosome.YInt ^= oneBits;
            //racunamo double vrijednost iz cijelobrojne
            chromosome.DXY();
        }
        //funkcija koja mutira x i y tako da x mutira u jednoj tacki a y u drugoj random tacki
        private static void MutationInRadnomDotsXY(ref ParameterFunction chromosome)
        {
            //imamo bit mutacije slucajno
            int dotMutationX= rand.Next(bitsNumberX), oneBitsX = 1;
            int dotMutationY= rand.Next(bitsNumberY), oneBitsY = 1;
            //racunamo cijebrojnu vrijednost x i y
            chromosome.Db();
            //setujemo onebits  na 1 bit koji zelimo da komplemeniramo 
            oneBitsX <<= dotMutationX;
            oneBitsY <<= dotMutationY;
            //komplementiramo ksorovanjem
            chromosome.XInt ^= oneBitsX;
            chromosome.YInt ^= oneBitsY;
            //racunamo double vrijednost iz cijelobrojne
            chromosome.DXY();
        }
        public static List<ParameterFunction> MutationChromosomes(List<ParameterFunction> chromosomes)
        {
            List<ParameterFunction> mutations = new List<ParameterFunction>();
            //za svaki hromozom ispitujemo da li ce da mutira u zavisnosti od parametra za mutaciju
            foreach(var chromosome in chromosomes)
            {
                double probabilityMutationIter = rand.NextDouble();
                //ispitujemo da li dozvoljavamo mutaciju
                ParameterFunction param = chromosome;
                if (probabilityMutationIter<probabilityMutation)
                {
                    Mutation(ref param);
                }
                mutations.Add(new ParameterFunction { X=param.X,Y=param.Y,XInt=param.XInt,YInt=param.YInt});
            }
            return mutations;
        }
        public static List<double> GetResultOfFunction(List<ParameterFunction> parammeters)
        {
            List<double> result = new List<double>();
            foreach(var param in parammeters)
            {
                double resultIter = function(param);
                Console.WriteLine("X= "+param.X+" y="+param.Y+" result:"+resultIter);
                result.Add(resultIter);
            }
            return result;
        }
        public static double function(ParameterFunction param)
        {
            double firstPart = 3 * (Math.Pow(1 - param.X, 2)) * (Math.Pow(Math.E, (-(Math.Pow(param.X, 2) + Math.Pow(param.Y + 1, 2)))));
            double secondPart = 10 * ((param.X / 5) - (Math.Pow(param.X, 3)) - (Math.Pow(param.Y, 5))) * (Math.Pow(Math.E, (-(Math.Pow(param.X, 2) + Math.Pow(param.Y, 2)))));
            double thirdPart = (1 / 3) * (Math.Pow(Math.E, (-(Math.Pow(param.X + 1, 2) + Math.Pow(param.Y, 2)))));
            return firstPart - secondPart - thirdPart;
            //return Math.Pow(param.X, 2) + Math.Pow(param.Y, 2);
        }
        public static List<ParameterFunction> InitializationTest()
        {
            List<ParameterFunction> parameters = new List<ParameterFunction>();
            for(int i = 0; i < populationNumber; i++)
            {
                parameters.Add(new ParameterFunction
                {
                    X=GetRandomDouble(GGx,gDx),
                    Y=GetRandomDouble(gGy,gDy)
                });
            }
            return parameters;
        }
    }
}
